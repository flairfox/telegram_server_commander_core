package ru.blodge.bserver.commander.telegram.view.docker;

import lombok.extern.slf4j.Slf4j;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import ru.blodge.bserver.commander.context.ApplicationContextAware;
import ru.blodge.bserver.commander.model.docker.DockerContainer;
import ru.blodge.bserver.commander.telegram.view.MessageContext;
import ru.blodge.bserver.commander.telegram.view.MessageView;
import ru.blodge.bserver.commander.utils.builders.InlineKeyboardBuilder;
import ru.blodge.bserver.commander.utils.factories.TelegramMessageFactory;

import static ru.blodge.bserver.commander.telegram.view.MenuRouter.*;
import static ru.blodge.bserver.commander.utils.Emoji.BACK_EMOJI;
import static ru.blodge.bserver.commander.utils.Emoji.REFRESH_EMOJI;

@Slf4j
public class DockerContainersListView implements MessageView, ApplicationContextAware {

    @Override
    public void display(MessageContext context) {

        InlineKeyboardBuilder keyboardBuilder = new InlineKeyboardBuilder();
        for (DockerContainer container : dockerService().getContainers()) {
            keyboardBuilder
                    .addButton(buildContainerCaption(container), buildContainerCallbackData(container))
                    .nextRow();
        }

        InlineKeyboardMarkup keyboard = keyboardBuilder
                .addButton(REFRESH_EMOJI + " Обновить", DOCKER_CONTAINERS_MENU_SELECTOR)
                .addButton(BACK_EMOJI + " Назад", MAIN_MENU_SELECTOR)
                .build();

        BotApiMethod<?> dockerContainers = TelegramMessageFactory.buildMessage(
                context.chatId(),
                context.messageId(),
                """
                        *Docker-контейнеры*

                        Вот какие контейнеры я нашел:
                        """,
                keyboard);

        try {
            commanderBot().execute(dockerContainers);
        } catch (TelegramApiException e) {
            log.error("Error executing docker containers list menu message", e);
        }
    }

    private String buildContainerCaption(DockerContainer container) {
        return container.status().statusEmoji() + " " + container.names();
    }

    private String buildContainerCallbackData(DockerContainer container) {
        return DOCKER_CONTAINER_INFO_MENU_SELECTOR + "." + container.id() + ".d";
    }

}
