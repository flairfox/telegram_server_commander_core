package ru.blodge.bserver.commander.telegram.handlers;

import lombok.extern.slf4j.Slf4j;
import org.telegram.telegrambots.meta.api.objects.Update;

import static ru.blodge.bserver.commander.telegram.view.MenuRouter.route;

@Slf4j
public class CallbackQueryHandler implements UpdateHandler {

    @Override
    public void handle(Update update) {
        log.debug("Received update, passing it to router...");
        route(update.getCallbackQuery());
    }

}
