package ru.blodge.bserver.commander.context;

import ru.blodge.bserver.commander.services.DockerService;
import ru.blodge.bserver.commander.services.SystemInfoService;
import ru.blodge.bserver.commander.telegram.CommanderBot;

public interface ApplicationContextAware {

    default ApplicationContext context() {
        return ApplicationContext.instance();
    }

    default CommanderBot commanderBot() {
        return context().commanderBot();
    }

    default DockerService dockerService() {
        return context().dockerService();
    }

    default SystemInfoService systemInfoService() {
        return context().systemInfoService();
    }

}
