FROM eclipse-temurin:21.0.1_12-jre-alpine
ENV ARTIFACT_NAME=beavers-server-commander-*.jar
COPY ./build/libs/$ARTIFACT_NAME /tmp/bsc.jar
WORKDIR /tmp
ENTRYPOINT ["java","-jar","bsc.jar"]