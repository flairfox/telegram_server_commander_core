package ru.blodge.bserver.commander.telegram.view;

import java.util.Arrays;
import java.util.Objects;

public record MessageContext(
        Long chatId,
        Integer messageId,
        String[] args
) {

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MessageContext context = (MessageContext) o;

        if (!Objects.equals(chatId, context.chatId)) return false;
        if (!Objects.equals(messageId, context.messageId)) return false;
        // Probably incorrect - comparing Object[] arrays with Arrays.equals
        return Arrays.equals(args, context.args);
    }

    @Override
    public int hashCode() {
        int result = chatId != null ? chatId.hashCode() : 0;
        result = 31 * result + (messageId != null ? messageId.hashCode() : 0);
        result = 31 * result + Arrays.hashCode(args);
        return result;
    }

    @Override
    public String toString() {
        return "MessageContext{" +
                "chatId=" + chatId +
                ", messageId=" + messageId +
                ", args=" + Arrays.toString(args) +
                '}';
    }

}
