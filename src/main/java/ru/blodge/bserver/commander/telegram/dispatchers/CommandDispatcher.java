package ru.blodge.bserver.commander.telegram.dispatchers;

import lombok.extern.slf4j.Slf4j;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import ru.blodge.bserver.commander.context.ApplicationContextAware;
import ru.blodge.bserver.commander.telegram.handlers.MenuCommandHandler;
import ru.blodge.bserver.commander.telegram.handlers.UpdateHandler;

import java.util.Map;

@Slf4j
public class CommandDispatcher implements UpdateDispatcher, ApplicationContextAware {

    private static final Map<String, UpdateHandler> updateHandlersMap = Map.of(
            "/menu", new MenuCommandHandler());

    @Override
    public void dispatch(Update update) {
        String command = update.getMessage().getText();

        if (command == null) {
            long chatId = update.getMessage().getChatId();
            SendMessage sendMessage = new SendMessage();
            sendMessage.setChatId(chatId);
            sendMessage.setText("Неизвестная команда!");

            try {
                commanderBot().execute(sendMessage);
            } catch (TelegramApiException e) {
                log.error("Error while sending message", e);
            }
        }

        updateHandlersMap.get(command).handle(update);
    }

}
