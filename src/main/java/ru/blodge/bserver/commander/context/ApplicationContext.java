package ru.blodge.bserver.commander.context;

import ru.blodge.bserver.commander.services.DockerService;
import ru.blodge.bserver.commander.services.SystemInfoService;
import ru.blodge.bserver.commander.telegram.CommanderBot;

public class ApplicationContext {

    private ApplicationContext() {}

    private static class Holder {
        private static final ApplicationContext applicationContext = new ApplicationContext();

        private static final CommanderBot commanderBot = new CommanderBot();
        private static final DockerService dockerService = new DockerService();
        private static final SystemInfoService systemInfoService = new SystemInfoService();
    }

    public static ApplicationContext instance() {
        return Holder.applicationContext;
    }

    public CommanderBot commanderBot() {
        return Holder.commanderBot;
    }

    public DockerService dockerService() {
        return Holder.dockerService;
    }

    public SystemInfoService systemInfoService() {
        return Holder.systemInfoService;
    }

}
