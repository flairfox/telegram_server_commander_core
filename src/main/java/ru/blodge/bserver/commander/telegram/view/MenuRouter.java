package ru.blodge.bserver.commander.telegram.view;

import lombok.extern.slf4j.Slf4j;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import ru.blodge.bserver.commander.telegram.view.docker.DockerContainerInfoView;
import ru.blodge.bserver.commander.telegram.view.docker.DockerContainersListView;
import ru.blodge.bserver.commander.telegram.view.system.DrivesInfoView;
import ru.blodge.bserver.commander.telegram.view.system.RebootMenuView;
import ru.blodge.bserver.commander.telegram.view.system.ResourceUtilizationView;
import ru.blodge.bserver.commander.telegram.view.system.ShutdownMenuView;

import java.util.Arrays;
import java.util.Map;

@Slf4j
public class MenuRouter {

    public static final String MAIN_MENU_SELECTOR = "1";
    public static final String DOCKER_CONTAINERS_MENU_SELECTOR = "2";
    public static final String DOCKER_CONTAINER_INFO_MENU_SELECTOR = "3";
    public static final String RESOURCE_UTILIZATION_MENU_SELECTOR = "5";
    public static final String DRIVES_INFO_MENU_SELECTOR = "6";
    public static final String REBOOT_MENU_SELECTOR = "7";
    public static final String SHUTDOWN_MENU_SELECTOR = "8";

    public static final Map<String, MessageView> viewSelectorMap = Map.of(
            MAIN_MENU_SELECTOR, new MainMenuView(),
            DOCKER_CONTAINERS_MENU_SELECTOR, new DockerContainersListView(),
            DOCKER_CONTAINER_INFO_MENU_SELECTOR, new DockerContainerInfoView(),
            RESOURCE_UTILIZATION_MENU_SELECTOR, new ResourceUtilizationView(),
            DRIVES_INFO_MENU_SELECTOR, new DrivesInfoView(),
            REBOOT_MENU_SELECTOR, new RebootMenuView(),
            SHUTDOWN_MENU_SELECTOR, new ShutdownMenuView()
    );

    private MenuRouter() {
    }

    public static void route(CallbackQuery callbackQuery) {
        log.debug("Received callback query with data {}.", callbackQuery.getData());
        String[] callbackData = callbackQuery.getData().split("\\.");
        String viewSelector = callbackData[0];
        MessageView messageView = viewSelectorMap.get(viewSelector);
        if (messageView == null) {
            log.error("No MessageView is available for selector {}", viewSelector);
        } else {
            String[] args = Arrays.copyOfRange(callbackData, 1, callbackData.length);
            MessageContext messageContext = new MessageContext(
                    callbackQuery.getMessage().getChatId(),
                    callbackQuery.getMessage().getMessageId(),
                    args);
            messageView.display(messageContext);
        }
    }

}
