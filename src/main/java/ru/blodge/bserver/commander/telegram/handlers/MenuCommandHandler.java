package ru.blodge.bserver.commander.telegram.handlers;

import org.telegram.telegrambots.meta.api.objects.Update;
import ru.blodge.bserver.commander.telegram.view.MessageContext;

import static ru.blodge.bserver.commander.telegram.view.MenuRouter.MAIN_MENU_SELECTOR;
import static ru.blodge.bserver.commander.telegram.view.MenuRouter.viewSelectorMap;

public class MenuCommandHandler implements UpdateHandler {

    @Override
    public void handle(Update update) {
        long chatId = update.getMessage().getChatId();

        viewSelectorMap
                .get(MAIN_MENU_SELECTOR)
                .display(new MessageContext(chatId, null, null));
    }

}
