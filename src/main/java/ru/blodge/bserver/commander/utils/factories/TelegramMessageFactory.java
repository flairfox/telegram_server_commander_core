package ru.blodge.bserver.commander.utils.factories;

import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;

public class TelegramMessageFactory {

    private TelegramMessageFactory() {}

    private static SendMessage buildSendMessage(
            Long chatId,
            String text,
            InlineKeyboardMarkup keyboard) {

        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(chatId);
        sendMessage.setParseMode("markdown");
        sendMessage.setText(text);
        sendMessage.setReplyMarkup(keyboard);

        return sendMessage;
    }

    public static EditMessageText buildEditMessage(
            Long chatId,
            Integer messageId,
            String text,
            InlineKeyboardMarkup keyboard) {

        EditMessageText editMessage = new EditMessageText();
        editMessage.setChatId(chatId);
        editMessage.setMessageId(messageId);
        editMessage.setParseMode("markdown");
        editMessage.setText(text);
        editMessage.setReplyMarkup(keyboard);

        return editMessage;
    }

    public static BotApiMethod<?> buildMessage(
            Long chatId,
            Integer messageId,
            String text,
            InlineKeyboardMarkup keyboard) {

        if (messageId == null) {
            return buildSendMessage(chatId, text, keyboard);
        } else {
            return buildEditMessage(chatId, messageId, text, keyboard);
        }
    }


}
