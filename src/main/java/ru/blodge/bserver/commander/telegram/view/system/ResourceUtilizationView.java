package ru.blodge.bserver.commander.telegram.view.system;

import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import lombok.extern.slf4j.Slf4j;
import ru.blodge.bserver.commander.context.ApplicationContextAware;
import ru.blodge.bserver.commander.model.system.MemoryUtilization;
import ru.blodge.bserver.commander.model.system.ResourceUtilizationInfo;
import ru.blodge.bserver.commander.telegram.view.MessageContext;
import ru.blodge.bserver.commander.telegram.view.MessageView;
import ru.blodge.bserver.commander.utils.builders.InlineKeyboardBuilder;
import ru.blodge.bserver.commander.utils.factories.TelegramMessageFactory;

import java.io.IOException;

import static ru.blodge.bserver.commander.telegram.view.MenuRouter.MAIN_MENU_SELECTOR;
import static ru.blodge.bserver.commander.telegram.view.MenuRouter.RESOURCE_UTILIZATION_MENU_SELECTOR;
import static ru.blodge.bserver.commander.utils.Emoji.BACK_EMOJI;
import static ru.blodge.bserver.commander.utils.Emoji.REFRESH_EMOJI;
import static ru.blodge.bserver.commander.utils.TextUtils.asciiProgressBar;
import static ru.blodge.bserver.commander.utils.TextUtils.humanReadableByteCountSI;

@Slf4j
public class ResourceUtilizationView implements MessageView, ApplicationContextAware {

    @Override
    public void display(MessageContext context) {

        InlineKeyboardMarkup keyboard = new InlineKeyboardBuilder()
                .addButton(REFRESH_EMOJI + " Обновить", RESOURCE_UTILIZATION_MENU_SELECTOR)
                .addButton(BACK_EMOJI + " Назад", MAIN_MENU_SELECTOR)
                .build();

        try {
            ResourceUtilizationInfo resourceUtilizationInfo = systemInfoService().getResourceUtilizationInfo();

            BotApiMethod<?> mainMenuMessage = TelegramMessageFactory.buildMessage(
                    context.chatId(),
                    context.messageId(),
                    """
                            *Утилизация CPU, RAM и SWAP*

                            Вот так загружена твоя система:

                            *CPU:*
                            `%s`
                            `Температура: %s°C`

                            *RAM:*
                            `%s`
                            *SWAP:*
                            `%s`
                            """.formatted(
                            asciiProgressBar(resourceUtilizationInfo.cpuUtilization().cpuLoadPercent()),
                            resourceUtilizationInfo.cpuUtilization().cpuTemperature(),
                            displayMemoryUtilization(resourceUtilizationInfo.memoryUtilization()),
                            displayMemoryUtilization(resourceUtilizationInfo.swapUtilization())),
                    keyboard);

            commanderBot().execute(mainMenuMessage);

        } catch (IOException e) {
            log.error("Error retrieving resource utilization info.", e);
        } catch (TelegramApiException e) {
            log.error("Error executing system menu message.", e);
        }

    }

    private String displayMemoryUtilization(MemoryUtilization memoryUtilization) {
        return """
                %s
                %s свободно из %s
                """.formatted(
                asciiProgressBar(memoryUtilization.percent()),
                humanReadableByteCountSI(memoryUtilization.free()),
                humanReadableByteCountSI(memoryUtilization.total()));
    }

}
