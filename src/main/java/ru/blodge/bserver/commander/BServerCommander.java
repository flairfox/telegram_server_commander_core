package ru.blodge.bserver.commander;

import lombok.extern.slf4j.Slf4j;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;

import ru.blodge.bserver.commander.context.ApplicationContext;

@Slf4j
public class BServerCommander {

    public static void main(String[] args) {
        try {
            TelegramBotsApi botsApi = new TelegramBotsApi(DefaultBotSession.class);
            botsApi.registerBot(ApplicationContext.instance().commanderBot());
            log.info("BServerCommander bot is up and running!");
        } catch (TelegramApiException e) {
            log.error("Error while starting BServerCommander bot!", e);
        }
    }

}
