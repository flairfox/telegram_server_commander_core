package ru.blodge.bserver.commander.utils;

public class Emoji {

    public static final String BACK_EMOJI = "◀️";
    public static final String REFRESH_EMOJI = "\uD83D\uDD04";

    public static final String GREEN_CIRCLE_EMOJI = "\uD83D\uDFE2";
    public static final String RED_CIRCLE_EMOJI = "\uD83D\uDD34";

    public static final String CONTAINER_EMOJI = "\uD83D\uDCE6";
    public static final String IMAGE_EMOJI = "\uD83D\uDDBC️";


    private Emoji() {
    }
}
