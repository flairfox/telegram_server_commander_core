package ru.blodge.bserver.commander.telegram.view;

import lombok.extern.slf4j.Slf4j;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import ru.blodge.bserver.commander.context.ApplicationContextAware;
import ru.blodge.bserver.commander.utils.builders.InlineKeyboardBuilder;
import ru.blodge.bserver.commander.utils.factories.TelegramMessageFactory;

import static ru.blodge.bserver.commander.telegram.view.MenuRouter.*;

@Slf4j
public class MainMenuView implements MessageView, ApplicationContextAware {

    @Override
    public void display(MessageContext context) {

        InlineKeyboardMarkup keyboard = new InlineKeyboardBuilder()
                .addButton("CPU, RAM и SWAP", RESOURCE_UTILIZATION_MENU_SELECTOR)
                .nextRow()
                .addButton("Дисках", DRIVES_INFO_MENU_SELECTOR)
                .nextRow()
                .addButton("Docker-контейнерах", DOCKER_CONTAINERS_MENU_SELECTOR)
                .nextRow()
                .addButton("Перезагрузить", REBOOT_MENU_SELECTOR)
                .nextRow()
                .addButton("Выключить", SHUTDOWN_MENU_SELECTOR)
                .build();

        BotApiMethod<?> mainMenuMessage = TelegramMessageFactory.buildMessage(
                context.chatId(),
                context.messageId(),
                """
                        *Главное меню*

                        Это главное меню твоего Сервера, здесь можно узнать о:
                        """,
                keyboard);

        try {
            commanderBot().execute(mainMenuMessage);
        } catch (TelegramApiException e) {
            log.error("Error executing main menu message", e);
        }
    }

}
