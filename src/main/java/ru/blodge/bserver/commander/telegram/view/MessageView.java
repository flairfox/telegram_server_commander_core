package ru.blodge.bserver.commander.telegram.view;

public interface MessageView {

    void display(MessageContext context);

}
