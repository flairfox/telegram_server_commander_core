package ru.blodge.bserver.commander.telegram.view.system;

import lombok.extern.slf4j.Slf4j;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import ru.blodge.bserver.commander.context.ApplicationContextAware;
import ru.blodge.bserver.commander.telegram.view.MessageContext;
import ru.blodge.bserver.commander.telegram.view.MessageView;
import ru.blodge.bserver.commander.utils.builders.InlineKeyboardBuilder;
import ru.blodge.bserver.commander.utils.factories.TelegramMessageFactory;

import java.io.IOException;
import java.util.Objects;

import static ru.blodge.bserver.commander.telegram.view.MenuRouter.MAIN_MENU_SELECTOR;
import static ru.blodge.bserver.commander.telegram.view.MenuRouter.SHUTDOWN_MENU_SELECTOR;
import static ru.blodge.bserver.commander.utils.Emoji.BACK_EMOJI;

@Slf4j
public class ShutdownMenuView implements MessageView, ApplicationContextAware {

    @Override
    public void display(MessageContext context) {

        if (context.args().length < 1) {
            displayConfirmation(context);
        } else if (Objects.equals(context.args()[0], "!")) {
            shutdownServer(context);
        }

    }

    private void displayConfirmation(MessageContext context) {

        InlineKeyboardMarkup keyboard = new InlineKeyboardBuilder()
                .addButton("Да", SHUTDOWN_MENU_SELECTOR + ".!")
                .addButton("Отмена", MAIN_MENU_SELECTOR)
                .build();

        BotApiMethod<?> rebootConfirmation = TelegramMessageFactory.buildMessage(
                context.chatId(),
                context.messageId(),
                """
                        *Завершение работы*

                        Действительно выключить сервер?
                        """,
                keyboard);

        try {
            commanderBot().execute(rebootConfirmation);

        } catch (TelegramApiException e) {
            log.error("Error executing shutdown confirmation menu message.", e);
        }
    }

    private void shutdownServer(MessageContext context) {

        InlineKeyboardMarkup keyboard = new InlineKeyboardBuilder()
                .addButton(BACK_EMOJI + " Назад", MAIN_MENU_SELECTOR)
                .build();

        try {
            BotApiMethod<?> mainMenuMessage = TelegramMessageFactory.buildMessage(
                    context.chatId(),
                    context.messageId(),
                    """
                            *Сервер будет выключен!*
                            """,
                    keyboard);

            commanderBot().execute(mainMenuMessage);

            systemInfoService().shutdown();

        } catch (IOException e) {
            log.error("Error shutting down system.", e);
        } catch (TelegramApiException e) {
            log.error("Error executing shut down menu message.", e);
        }

    }
}
