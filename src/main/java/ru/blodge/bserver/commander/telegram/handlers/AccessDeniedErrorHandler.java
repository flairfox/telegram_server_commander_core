package ru.blodge.bserver.commander.telegram.handlers;

import lombok.extern.slf4j.Slf4j;
import org.telegram.telegrambots.meta.api.methods.send.SendVideo;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.blodge.bserver.commander.context.ApplicationContextAware;

import java.io.IOException;
import java.io.InputStream;

import static ru.blodge.bserver.commander.configuration.TelegramBotConfig.ACCESS_DENIED_FILE;

@Slf4j
public class AccessDeniedErrorHandler implements UpdateHandler, ApplicationContextAware {

    private static String accessDeniedFileId;

    public void handle(Update update) {
        log.warn("User with ID {} is not an administrator", update.getMessage().getFrom().getId());

        // Сообщение на которое нужно отправить ответ
        Message message = update.getMessage();

        // Файл с видео-ответом
        InputFile inputFile = new InputFile();
        if (accessDeniedFileId == null) {
            // todo Кэширование в temp
            // Загружаем файл с видеоответом, если ID не закэширован
            ClassLoader classLoader = getClass().getClassLoader();
            try (InputStream inputStream = classLoader.getResourceAsStream(ACCESS_DENIED_FILE)) {
                inputFile.setMedia(inputStream, ACCESS_DENIED_FILE);
                sendAccessDeniedResponse(message, inputFile);
            } catch (IOException e) {
                log.error("Error while loading {}", ACCESS_DENIED_FILE);
            }
        } else {
            // Если ID файла был закэширован, просто отправляем его по ID
            inputFile.setMedia(accessDeniedFileId);
            sendAccessDeniedResponse(message, inputFile);
        }
    }

    private void sendAccessDeniedResponse(Message message, InputFile responseFile) {
        // Создаем сообщение-видеоответ
        SendVideo accessDeniedVideo = new SendVideo();
        accessDeniedVideo.setChatId(message.getChatId());
        accessDeniedVideo.setVideo(responseFile);

        try {
            Message sentMessage = commanderBot().execute(accessDeniedVideo);
            // Обновляем в кэше ID файла видеоответа
            accessDeniedFileId = sentMessage.getVideo().getFileId();
        } catch (TelegramApiException e) {
            log.error("Error while sending response to {}", message);
        }
    }

}
