package ru.blodge.bserver.commander.telegram.view.system;

import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import lombok.extern.slf4j.Slf4j;
import ru.blodge.bserver.commander.context.ApplicationContextAware;
import ru.blodge.bserver.commander.model.system.DriveInfo;
import ru.blodge.bserver.commander.telegram.view.MessageContext;
import ru.blodge.bserver.commander.telegram.view.MessageView;
import ru.blodge.bserver.commander.utils.builders.InlineKeyboardBuilder;
import ru.blodge.bserver.commander.utils.factories.TelegramMessageFactory;

import java.io.IOException;
import java.util.List;

import static ru.blodge.bserver.commander.telegram.view.MenuRouter.DRIVES_INFO_MENU_SELECTOR;
import static ru.blodge.bserver.commander.telegram.view.MenuRouter.MAIN_MENU_SELECTOR;
import static ru.blodge.bserver.commander.utils.Emoji.BACK_EMOJI;
import static ru.blodge.bserver.commander.utils.Emoji.REFRESH_EMOJI;
import static ru.blodge.bserver.commander.utils.TextUtils.asciiProgressBar;
import static ru.blodge.bserver.commander.utils.TextUtils.humanReadableByteCountSI;

@Slf4j
public class DrivesInfoView implements MessageView, ApplicationContextAware {

    @Override
    public void display(MessageContext context) {

        InlineKeyboardMarkup keyboard = new InlineKeyboardBuilder()
                .addButton(REFRESH_EMOJI + " Обновить", DRIVES_INFO_MENU_SELECTOR)
                .addButton(BACK_EMOJI + " Назад", MAIN_MENU_SELECTOR)
                .build();

        try {
            List<DriveInfo> drivesInfo = systemInfoService().getDrivesInfo();

            StringBuilder drivesInformation = new StringBuilder();
            for (DriveInfo driveInfo : drivesInfo) {
                drivesInformation.append("*").append(driveInfo.mountPoint()).append("*")
                        .append("\n")
                        .append("`").append(asciiProgressBar(driveInfo.percent())).append("`")
                        .append("\n")
                        .append("`").append(humanReadableByteCountSI(driveInfo.free())).append(" свободно из ")
                        .append(humanReadableByteCountSI(driveInfo.total())).append("`")
                        .append("\n");
            }

            BotApiMethod<?> mainMenuMessage = TelegramMessageFactory.buildMessage(
                    context.chatId(),
                    context.messageId(),
                    """
                            *Информация о дисках*

                            Вот такие устройства хранения я нашел в твоей системе:

                            %s
                            """.formatted(drivesInformation),
                    keyboard);

            commanderBot().execute(mainMenuMessage);

        } catch (IOException e) {
            log.error("Error retrieving resource utilization info.", e);
        } catch (TelegramApiException e) {
            log.error("Error executing system menu message.", e);
        }

    }

}
