package ru.blodge.bserver.commander.telegram;

import lombok.extern.slf4j.Slf4j;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.blodge.bserver.commander.telegram.dispatchers.TelegramUpdateDispatcher;
import ru.blodge.bserver.commander.telegram.dispatchers.UpdateDispatcher;

import static ru.blodge.bserver.commander.configuration.TelegramBotConfig.TELEGRAM_BOT_TOKEN;
import static ru.blodge.bserver.commander.configuration.TelegramBotConfig.TELEGRAM_BOT_USERNAME;

@Slf4j
public class CommanderBot extends TelegramLongPollingBot {

    private final UpdateDispatcher dispatcher = new TelegramUpdateDispatcher();

    public CommanderBot() {
        super(TELEGRAM_BOT_TOKEN);
    }

    @Override
    public void onUpdateReceived(Update update) {
        log.info("Received new update {}", update);
        dispatcher.dispatch(update);
    }

    @Override
    public String getBotUsername() {
        return TELEGRAM_BOT_USERNAME;
    }

}
